import argparse
import io


def convert(input_file: io.TextIOWrapper, output_file: io.TextIOWrapper, name: str):
    banner = (
        '"'
        + str(("\n\r" + input_file.read().replace("\n", "\n\r")).encode("utf-8"))[2:-1]
        + '"'
    )

    _ = output_file.write(f"#ifndef {name.upper()}_H_INCLUDED\n")
    _ = output_file.write(f"#define {name.upper()}_H_INCLUDED\n\n")
    _ = output_file.write(f"#include <stdint.h>\n\n")

    _ = output_file.write(f"const uint8_t {name}[] = {banner};\n\n")

    _ = output_file.write(f"#endif // {name.upper()}_H_INCLUDED\n")


def main():
    parser = argparse.ArgumentParser(description="Convert banner .txt file to .h file")
    _ = parser.add_argument(
        "input_file", type=argparse.FileType("r"), help="Input .txt file"
    )
    _ = parser.add_argument(
        "output_file", type=argparse.FileType("w+"), help="Output .h file"
    )
    _ = parser.add_argument(
        "--name", type=str, help="Name for the variable", default="header"
    )

    args = parser.parse_args()
    convert(args.input_file, args.output_file, args.name)


if __name__ == "__main__":
    main()
