Etendard is a command line utility to embed text files inside C/C++ code.
This is useful for creating banner text on embedded projects.


```
usage: etendard [-h] [--name NAME] input_file output_file

Convert banner .txt file to .h file

positional arguments:
  input_file   Input .txt file
  output_file  Output .h file

options:
  -h, --help   show this help message and exit
  --name NAME  Name for the variable
```